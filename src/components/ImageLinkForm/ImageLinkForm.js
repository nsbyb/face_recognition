import React from 'react';
import './ImageLinkForm.css'


const ImageLinkForm = ({onInputChange ,onSubmit}) =>{
return(
	<div>
		<p className='f3'>
{'This Magic Rocket will detect faces in your picture'}

	</p>	
	<div className='center form'>
	<div className='center pa4 br3 shadow-4'>
		<input className='f4 pa2 w-70 center' type='tex' onChange = {onInputChange} />
		<button onClick={onSubmit} className='w-30 grow f4 link 
		ph3 pv2 dib white bg-light-purple'>
		 Detect</button>
		 </div>
			
	</div>
	</div>

	);
}

export default ImageLinkForm;


